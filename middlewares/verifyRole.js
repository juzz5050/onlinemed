function verifyRole(allowedRoles) {
    return function (req, res, next) {
        if (allowedRoles.includes(req.user.role)) {
            next(); // role is correct, proceed
        } else {
            res.status(403).send('Permission denied.');
        }
    };
}

module.exports = verifyRole;
