const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const db = require('./config/db.config');
const userRoutes = require('./routes/users');


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet());
app.use('/api/users', userRoutes);

app.get('/', (req, res) => {
    res.send('yes its working');
});

db.connect((err) => {
    if (err) throw err;
    console.log('Connected to the database.');
});

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
