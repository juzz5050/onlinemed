const bcrypt = require('bcrypt');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Enter the password to be hashed: ', (password) => {
    const hashedPassword = bcrypt.hashSync(password, 10); // 10 is the salt rounds
    console.log('Hashed Password:', hashedPassword);
    rl.close();
});
