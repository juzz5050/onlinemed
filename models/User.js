class User {
    constructor(id, name, email, password, role, mobile, address, created_at, updated_at) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.mobile = mobile;
        this.address = address;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}

module.exports = User;
