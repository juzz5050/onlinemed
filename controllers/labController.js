const db = require('../config/db.config');

exports.addLabDetails = (req, res) => {
    const { patient_id, file_path, description } = req.body;
    const date_uploaded = new Date().toISOString().slice(0, 19).replace('T', ' ');

    const query = 'INSERT INTO labfiles (patient_id, file_path, description, date_uploaded) VALUES (?, ?, ?, ?)';
    db.query(query, [patient_id, file_path, description, date_uploaded], (error, results) => {
        if (error) {
            console.error(error.message);
            res.status(500).send({ message: "Error uploading lab details." });
        } else {
            res.status(200).send({ message: "Lab details successfully uploaded." });
        }
    });
};
