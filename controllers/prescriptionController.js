const db = require('../config/db.config');

exports.addPrescription = (req, res) => {
    const { patient_id, doctor_id, Prescription, date_prescribed } = req.body;

    const sql = "INSERT INTO Prescriptions (patient_id, doctor_id, Prescription, date_prescribed) VALUES (?, ?, ?, ?)";
    
    db.query(sql, [patient_id, doctor_id, Prescription, date_prescribed], (err, result) => {
        if (err) {
            return res.status(500).send({ message: err.message });
        }
        res.status(200).send({ message: 'Prescription added successfully!' });
    });
};
