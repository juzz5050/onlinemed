const db = require('../config/db.config');

exports.addPharmacyReport = (req, res) => {
    const { patient_id, medicine_name, quantity } = req.body;
    const date_given = new Date().toISOString().slice(0, 19).replace('T', ' ');

    // Handle multiple medicines
    if (Array.isArray(medicine_name) && Array.isArray(quantity) && medicine_name.length === quantity.length) {
        const queries = [];

        for (let i = 0; i < medicine_name.length; i++) {
            queries.push([patient_id, medicine_name[i], quantity[i], date_given]);
        }

        const sql = 'INSERT INTO pharmacy (patient_id, medicine_name, quantity, date_given) VALUES ?';
        db.query(sql, [queries], (error) => {
            if (error) {
                console.error(error.message);
                res.status(500).send({ message: "Error adding pharmacy report." });
            } else {
                res.status(200).send({ message: "Pharmacy report successfully added." });
            }
        });
    } else {
        res.status(400).send({ message: "Invalid input data." });
    }
};
