const db = require('../config/db.config');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.registerUser = (req, res) => {
    const { name, email, password, role, mobile, address } = req.body;
    const hashedPassword = bcrypt.hashSync(password, 8);

    const query = "INSERT INTO users (name, email, password, role, mobile, address) VALUES (?, ?, ?, ?, ?, ?)";

    db.query(query, [name, email, hashedPassword, role, mobile, address], (err, result) => {
        if (err) {
            return res.status(500).send({ message: err });
        }
        res.status(201).send({ message: 'User registered successfully!' });
    });
};

exports.loginUser = (req, res) => {
    const { email, password } = req.body;

    // Check if email and password are provided
    if (!email || !password) {
        return res.status(400).send({ message: 'Email and password are required!' });
    }

    const query = "SELECT * FROM users WHERE email = ?";

    db.query(query, [email], (err, results) => {
        if (err) {
            return res.status(500).send({ message: err });
        }

        if (results.length === 0) {
            return res.status(404).send({ message: 'User not found!' });
        }

        const user = results[0];

        const passwordIsValid = bcrypt.compareSync(password, user.password);

        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: 'Invalid Password!'
            });
        }

        const token = jwt.sign({ id: user.id, role: user.role, name: user.name, email: user.email }, process.env.JWT_SECRET, {
            expiresIn: 86400 // 24 hours
        });

        res.status(200).send({
            id: user.id,
            name: user.name,
            email: user.email,
            role: user.role,
            accessToken: token
        });
    });
};

exports.getDoctorDetails = (req, res) => {
    const doctorId = req.params.doctorId;
    const query = "SELECT id, name, email, mobile FROM users WHERE role = 'doctor' AND id = ?";

    db.query(query, [doctorId], (err, result) => {
        if (err) {
            return res.status(500).send({ message: err.message });
        }

        if (result.length === 0) {
            return res.status(404).send({ message: "Doctor not found!" });
        }
        
        res.status(200).send(result[0]);
    });
};


exports.listPatients = (req, res) => {
    const sql = "SELECT id,name, email, mobile, address FROM users WHERE role = 'patient'";

    db.query(sql, (err, result) => {
        if (err) {
            return res.status(500).send({ message: err.message });
        }
        res.status(200).send(result);
    });
};

exports.getPatientDetails = (req, res) => {
    const patientId = req.params.patientId;

    const patientQuery = 'SELECT name, email, mobile FROM users WHERE id = ? AND role = "patient"';

    db.query(patientQuery, [patientId], (error, results) => {
        if (error) {
            return res.status(500).send({ message: "Error fetching patient details." });
        }

        if (results.length === 0) {
            return res.status(404).send({ message: "Patient not found." });
        }

        res.status(200).send(results[0]);  // Sending back the first (and only) result since it's an array.
    });
};

exports.getPatientReport = (req, res) => {
    const patientId = req.params.patientId;
    
    // Start with fetching basic patient details
    const userQuery = 'SELECT name, email, mobile FROM users WHERE id = ?';
    
    db.query(userQuery, [patientId], (error, patientInfo) => {
        if (error) {
            return res.status(500).send({ message: "Error fetching patient details." });
        }

        if (patientInfo.length === 0) {
            return res.status(404).send({ message: "Patient not found." });
        }

        // Fetch prescriptions
        const prescriptionsQuery = 'SELECT * FROM prescriptions WHERE patient_id = ?';
        
        db.query(prescriptionsQuery, [patientId], (error, prescriptions) => {
            if (error) {
                return res.status(500).send({ message: "Error fetching prescriptions." });
            }

            // Fetch lab reports
            const labReportsQuery = 'SELECT * FROM labfiles WHERE patient_id = ?';
            db.query(labReportsQuery, [patientId], (error, labReports) => {
                if (error) {
                    return res.status(500).send({ message: "Error fetching lab reports." });
                }

                // Fetch pharmacy reports
                const pharmacyReportsQuery = 'SELECT * FROM pharmacy WHERE patient_id = ?';
                db.query(pharmacyReportsQuery, [patientId], (error, pharmacyReports) => {
                    if (error) {
                        return res.status(500).send({ message: "Error fetching pharmacy reports." });
                    }

                    // Combine all the data and send
                    res.status(200).send({
                        patientDetails: patientInfo[0],  // since it's an array, we pick the first (and only) item
                        prescriptions: prescriptions,
                        labReports: labReports,
                        pharmacyReports: pharmacyReports
                    });
                });
            });
        });
    });
};

