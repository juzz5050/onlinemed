const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const prescriptionController = require('../controllers/prescriptionController');
const labController = require('../controllers/labController');
const pharmacyController = require('../controllers/pharmacyController');
const authenticateJWT = require('../middlewares/auth');
const verifyRole = require('../middlewares/verifyRole');

// role based access implemented
router.post('/register', authenticateJWT, verifyRole('admin'), userController.registerUser);
router.post('/login', userController.loginUser);
router.get('/patients', authenticateJWT, verifyRole(['admin', 'doctor', 'pharmacist', 'lab_technician']), userController.listPatients);
router.post('/prescriptions', authenticateJWT, verifyRole(['doctor']), prescriptionController.addPrescription);
router.post('/add-lab-details', authenticateJWT, verifyRole('lab_technician'), labController.addLabDetails);
router.post('/add-pharmacy-report', authenticateJWT, verifyRole('pharmacist'), pharmacyController.addPharmacyReport);
router.get('/patient-report/:patientId', authenticateJWT, verifyRole('doctor'), userController.getPatientReport);
router.get('/patient-details/:patientId', authenticateJWT, verifyRole(['admin', 'doctor']), userController.getPatientDetails);
router.get('/doctor-details/:doctorId', authenticateJWT, verifyRole(['admin', 'patient', 'doctor']), userController.getDoctorDetails);



module.exports = router;
